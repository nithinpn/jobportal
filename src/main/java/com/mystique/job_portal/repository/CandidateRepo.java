package com.mystique.job_portal.repository;
import com.mystique.job_portal.entity.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidateRepo extends JpaRepository<Candidate, Integer> {

}
