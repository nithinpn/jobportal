package com.mystique.job_portal.repository;
import com.mystique.job_portal.entity.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
public interface JobRepository extends JpaRepository<Jobs , Integer> {


}
