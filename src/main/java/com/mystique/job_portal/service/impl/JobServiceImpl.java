package com.mystique.job_portal.service.impl;

import com.mystique.job_portal.commons.JobConstants;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.exception.JobNotFoundException;

import com.mystique.job_portal.repository.JobRepository;
import com.mystique.job_portal.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private JobRepository jobRepository;


    /**
     *
     * Method for creating new job
     * @param jobs ~
     * @return jobs ~
     */
    @Override
    public Jobs addJobs(Jobs jobs) {
        return jobRepository.save(jobs);
    }

    /**
     *
     * Method for find all jobs
     * @return list of jobs
     */

    @Override
    public List<Jobs> getAllJobs() {
        List<Jobs> jobs = new ArrayList<>();
        jobRepository.findAll().forEach(jobs::add);
        return jobs;
    }

    /**
     *
     * Method for find jobs by jobId
     * @param jobId ~
     * @return Jobs ~
     * @throws JobNotFoundException ~
     */

    @Override
    public Jobs getJobsById(int jobId) throws JobNotFoundException {
        return jobRepository.findById(jobId).orElseThrow
                (() -> new JobNotFoundException(JobConstants.ERROR_MESSAGE.getValue() + jobId));
    }

    /**
     *
     * Method for update job
     * @param jobs ~
     * @return Jobs ~
     * @throws JobNotFoundException ~
     */

    @Override
    public Jobs updateJob(Jobs jobs) throws JobNotFoundException {
        Jobs exist = jobRepository.findById(jobs.getJobId()).orElseThrow
                (() -> new JobNotFoundException(JobConstants.ERROR_MESSAGE.getValue() + jobs.getJobId()));
        exist.setJobName(jobs.getJobName());
        exist.setJobDescription(jobs.getJobDescription());
        exist.setCreatedDate(jobs.getCreatedDate());
        exist.setLastModifiedDate(jobs.getLastModifiedDate());
        exist.setStatus(jobs.getStatus());
        return jobRepository.save(exist);
    }

    /**
     *
     * Method for delete job
     * @param jobId ~
     * @throws JobNotFoundException ~
     */

    @Override
    public void deleteJob(int jobId) throws JobNotFoundException {
        Optional<Jobs> jobs = Optional.ofNullable(jobRepository.findById(jobId).orElseThrow
                (() -> new JobNotFoundException(JobConstants.ERROR_MESSAGE.getValue())));
        jobRepository.deleteById(jobId);
    }

}
