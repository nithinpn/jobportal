package com.mystique.job_portal.service.impl;


import com.mystique.job_portal.entity.Candidate;
import com.mystique.job_portal.repository.CandidateRepo;
import com.mystique.job_portal.service.CandidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Slf4j
@Service
public class CandidateServiceImpl implements CandidateService {
    @Autowired
    private CandidateRepo candidateRepo;

    /**
     * Method for apply for a job
     *
     * @param candidate ~
     * @return Candidate ~
     */

    @Override
    public Candidate apply(Candidate candidate) {

        log.info("Candidate saved");
        return candidateRepo.save(candidate);
    }
}
