package com.mystique.job_portal.service;

import com.mystique.job_portal.entity.Candidate;

public interface CandidateService {
    Candidate apply(Candidate candidate);
}
