package com.mystique.job_portal.service;

import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.exception.JobNotFoundException;

import java.util.List;

public interface JobService {
    Jobs addJobs(Jobs jobs);

    List<Jobs> getAllJobs();

    Jobs getJobsById(int jobId) throws JobNotFoundException;

    Jobs updateJob(Jobs jobs) throws JobNotFoundException;

    void deleteJob(int jobId) throws JobNotFoundException;
}
