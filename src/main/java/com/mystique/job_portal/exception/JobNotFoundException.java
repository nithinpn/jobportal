package com.mystique.job_portal.exception;

public class JobNotFoundException extends Exception {
    /**
     * Method for Exception
     *
     * @param message
     */
    public JobNotFoundException(String message) {
        super(message);
    }
}
