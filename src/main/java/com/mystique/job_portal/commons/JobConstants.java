package com.mystique.job_portal.commons;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobConstants {

    TRUE("true"),
    ACTIVE("Active"),
    INACTIVE("Inactive"),
    ERROR_MESSAGE("Job not found with the given jobId"),
    UPDATE_MESSAGE("updated successfully"),
    DELETE_MESSAGE("deleted successfully");


    private final String value;
}
