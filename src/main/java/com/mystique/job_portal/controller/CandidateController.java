package com.mystique.job_portal.controller;

import com.mystique.job_portal.commons.JobConstants;
import com.mystique.job_portal.entity.Candidate;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.entity.dto.CandidateDto;
import com.mystique.job_portal.exception.JobNotFoundException;
import com.mystique.job_portal.repository.JobRepository;
import com.mystique.job_portal.service.CandidateService;
import com.mystique.job_portal.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.FOUND;
@Slf4j
@RestController
@RequestMapping("/jpc/jobs")
public class CandidateController {
    CandidateDto candidateDto;

    @Autowired
    private JobService jobservice;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private CandidateService candidateService;


    /**
     * Method for find all jobs
     *
     * @return List of jobs ~
     */

    @GetMapping("/getAll")
    public List<Jobs> findAllJobs() {
        log.info("finding jobs");
        return jobservice.getAllJobs();
    }

    /**
     * Method for finding active jobs
     *
     * @return List of active jobs~
     */

    @GetMapping("/active")
    public List<Jobs> findActiveJobs() {

        log.info("Showing active jobs");
        return jobservice.getAllJobs().stream().
                filter(jobs -> jobs.getStatus().equals(JobConstants.ACTIVE.getValue())).collect(Collectors.toList());

    }

    /**
     * Method for finding inactive jobs
     *
     * @return List of inactive jobs ~
     */

    @GetMapping("/inactive")
    public List<Jobs> findInActiveJobs() {

        log.info("Showing inactive jobs");
        return jobservice.getAllJobs()
                .stream()
                .filter(jobs -> jobs.getStatus().equals(JobConstants.INACTIVE.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * Method for find active jobs by jobId
     *
     * @param jobId ~
     * @return Jobs ~
     * @throws JobNotFoundException ~
     */

    @GetMapping("/get/{jobId}")
    public ResponseEntity<Jobs> findJobsById(@PathVariable int jobId) throws JobNotFoundException {

        log.info("Finding jobs with jobId");
        Jobs jobs = jobservice.getJobsById(jobId);
        if (jobs.getStatus().equals(JobConstants.ACTIVE.getValue())) {
            return new ResponseEntity<>(jobs, FOUND);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    /**
     * Method for applying for a job
     *
     * @param candidateDto ~
     * @param jobId      ~
     * @return String ~
     * @throws JobNotFoundException ~
     */
    @PostMapping("/{jobId}/apply")
    public ResponseEntity<String> applyJobs(@RequestBody CandidateDto candidateDto, @PathVariable int jobId)
            throws JobNotFoundException {

        Optional<Object> candidate = Optional.of(new Candidate(candidateDto));
        log.info("applying for a job");
       candidate =  jobRepository.findById(jobId).map(jobs -> {
            jobs.getCandidate().add(candidateDto);
            return candidateService.apply(candidateDto);
        });

        return ResponseEntity.ok().body(JobConstants.ERROR_MESSAGE.getValue() + jobId);
    }
}