package com.mystique.job_portal.controller;

import com.mystique.job_portal.commons.JobConstants;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.entity.dto.JobDto;
import com.mystique.job_portal.exception.JobNotFoundException;
import com.mystique.job_portal.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;


import static org.springframework.http.HttpStatus.CREATED;


/**
 * @author Nithin
 */
@Slf4j
@RestController
@RequestMapping("/jp/jobs")

public class JobController {
    JobDto jobDto;


    @Autowired
    private JobService jobservice;

    /**
     * Method for creating jobs
     *
     * @param jobDto   ~
     * @param isAdmin ~
     * @return Return jobs ~
     */
    @PostMapping("/add")
    public ResponseEntity<Jobs> createJobs(@RequestBody JobDto jobDto,
                                           @RequestParam(value = "isAdmin") String isAdmin) {
        Jobs jobs = new Jobs(jobDto);

        log.info("Creating new job");
        if (isAdmin.equals(JobConstants.TRUE.getValue())) {
            jobs = jobservice.addJobs(jobs);
            return new ResponseEntity<>(jobs, CREATED);
        } else
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Method for get all jobs
     *
     * @param isAdmin ~
     * @return returns list of jobs ~
     */

    @GetMapping("/getAll")
    public ResponseEntity<List<Jobs>> findAllJobs(@RequestParam(value = "isAdmin") String isAdmin) {
        log.info("Find all jobs");
        if (isAdmin.equals(JobConstants.TRUE.getValue())) {
            return new ResponseEntity<>(jobservice.getAllJobs(), HttpStatus.FOUND);
        } else

            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Method for find jobs with job-id
     *
     * @param jobId   ~
     * @param isAdmin ~
     * @return Job with  Http status ~
     * @throws JobNotFoundException ~
     */

    @GetMapping("get/{jobId}")
    public ResponseEntity<Jobs> findJobsById(@PathVariable int jobId, @RequestParam(value = "isAdmin") String isAdmin)
            throws JobNotFoundException {

        log.info("Find jobs by id");
        if (isAdmin.equals(JobConstants.TRUE.getValue())) {
            return new ResponseEntity<>(jobservice.getJobsById(jobId), HttpStatus.FOUND);
        } else
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Method for updating Jobs
     *
     * @param jobDto    ~
     * @param isAdmin ~
     * @return String ~
     * @throws JobNotFoundException ~
     */

    @PutMapping("/update")
    public ResponseEntity<String> updateJobs(@RequestBody JobDto jobDto, @RequestParam(value = "isAdmin") String isAdmin) throws JobNotFoundException {
        if (isAdmin.equals(JobConstants.TRUE.getValue())) {
            Jobs jobs = new Jobs(jobDto);

            log.info("Updating jobs");
            jobservice.updateJob(jobs);
            return ResponseEntity.ok().body(JobConstants.UPDATE_MESSAGE.getValue());
        } else
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Method for deleting the job with given jobId
     *
     * @param jobId   ~
     * @param isAdmin ~
     * @return String ~
     * @throws JobNotFoundException ~
     */

    @DeleteMapping("/delete/{jobId}")
    public ResponseEntity<String> deleteJobById(@PathVariable int jobId,
                                                @RequestParam(value = "isAdmin") String isAdmin)
            throws JobNotFoundException {
        log.info("Deleting jobs");
        if (isAdmin.equals(JobConstants.TRUE.getValue())) {
            jobservice.deleteJob(jobId);
            return ResponseEntity.ok().body(JobConstants.DELETE_MESSAGE.getValue());
        } else
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
