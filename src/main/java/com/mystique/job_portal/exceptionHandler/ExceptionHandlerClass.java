package com.mystique.job_portal.exceptionHandler;

import com.mystique.job_portal.exception.JobNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerClass {
    /**
     * Method for handling exception
     *
     * @param e ~
     * @return String ~
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(JobNotFoundException.class)

    public Map<String, String> handleGetByIdException(JobNotFoundException e) {
        Map<String, String> errormap = new HashMap<>();
        errormap.put("errorMessage", e.getMessage());
        return errormap;

    }
}
