package com.mystique.job_portal.entity;

import com.mystique.job_portal.entity.dto.JobDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Jobs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int jobId;
    private String jobName;
    private String jobDescription;
    private String createdDate;
    private String lastModifiedDate;
    private String status;

    @OneToMany(targetEntity = Candidate.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "jobId", referencedColumnName = "jobId")
    private List<Candidate> candidate;


    public Jobs() {
    }

    public Jobs(int jobId, String jobName, String jobDescription, String createdDate,
                String lastModifiedDate, String status) {
        this.jobId = jobId;
        this.jobName = jobName;
        this.jobDescription = jobDescription;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
        this.status = status;
    }

    /**
     *
     * @param jobDto
     */
    public Jobs(JobDto jobDto) {
    }
}