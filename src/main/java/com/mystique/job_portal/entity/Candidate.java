package com.mystique.job_portal.entity;


import com.mystique.job_portal.entity.dto.CandidateDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int candidateId;
    private String candidateName;
    private String dateOfBirth;
    private long phoneNumber;
    private String location;
    private String email;
    private int zipCode;

    public Candidate(CandidateDto candidateDto) {
    }
}
