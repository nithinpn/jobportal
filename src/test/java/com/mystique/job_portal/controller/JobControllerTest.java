package com.mystique.job_portal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.service.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class JobControllerTest {
    private Jobs jobs;

    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer();
    @Mock
    private JobService jobService;

    @InjectMocks
    private JobController jobController;


    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(jobController).build();
        jobs = new Jobs(1, "peon", "sslc", "2022-08-08",
                "2022-09-06", "active");
    }

    @Test
    public void testCreateJobsIsAdminTrue() throws Exception {
        String jsonRequest = objectMapper.writeValueAsString(jobs);
        this.mockMvc.perform(post("/jp/jobs/add?isAdmin=true")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateJobsIsAdminFalse() throws Exception {
        String jsonRequest = objectMapper.writeValueAsString(jobs);
        this.mockMvc.perform(post("/jp/jobs/add?isAdmin=false")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testFindAllJobsIsAdminTrue() throws Exception {
        List<Jobs> jobs = new ArrayList<>(Arrays.asList
                ((new Jobs(10, "Engineer", "4year experience",
                                "2022-02-03", "2022,02,09", "Active")),
                        (new Jobs(11, "civil Engineer", "11year experience",
                                "2022-08-13", "2022,08,25", "Active"))));
        when(jobService.getAllJobs()).thenReturn(jobs);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jp/jobs/getAll?isAdmin=true")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$[0].jobName", is("Engineer")));
    }

    @Test
    public void testFindAllJobsIsAdminFalse() throws Exception {
        List<Jobs> jobs = new ArrayList<>(Arrays.asList
                ((new Jobs(10, "Engineer", "4year experience",
                                "2022-02-03", "2022,02,09", "Active")),
                        (new Jobs(11, "civil Engineer", "11year experience",
                                "2022-08-13", "2022,08,25", "Active"))));
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jp/jobs/getAll?isAdmin=false")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testFindJobsByIdIsAdminTrue() throws Exception {
        int jobId = 1;


        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jp/jobs/get/10?isAdmin=true")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
        Assertions.assertEquals(1, jobs.getJobId());

    }

    @Test
    public void testFindJobsByIdIsAdminFalse() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jp/jobs/get/10?isAdmin=false")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateJobsIsAdminTrue() throws Exception {
        String jsonRequest = objectMapper.writeValueAsString(jobs);
        this.mockMvc.perform(put("/jp/jobs/update?isAdmin=true")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        Assertions.assertEquals(1, jobs.getJobId());
    }

    @Test
    public void testUpdateJobsIsAdminFalse() throws Exception {
        String jsonRequest = objectMapper.writeValueAsString(jobs);
        this.mockMvc.perform(put("/jp/jobs/update?isAdmin=false")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteJobByIdIsAdminTrue() throws Exception {
        int jobId = 1;
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder = MockMvcRequestBuilders.delete("/jp/jobs/delete/1?isAdmin=true")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(status().isOk());


    }

    @Test
    public void testDeleteJobByIdIsAdminFalse() throws Exception {
        int jobId = 1;
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder = MockMvcRequestBuilders.delete("/jp/jobs/delete/1?isAdmin=false")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(status().isForbidden());


    }
}