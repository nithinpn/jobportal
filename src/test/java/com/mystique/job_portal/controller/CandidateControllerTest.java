package com.mystique.job_portal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mystique.job_portal.commons.JobConstants;
import com.mystique.job_portal.entity.Candidate;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.repository.JobRepository;
import com.mystique.job_portal.service.JobService;
import com.mystique.job_portal.service.impl.CandidateServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class CandidateControllerTest {
    private MockMvc mockMvc;
    private List<Jobs> jobs;

    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer();
    @Mock
    private CandidateServiceImpl candidateService;
    @Mock
    private JobService jobService;
    @Mock
    private JobRepository jobRepository;
    @InjectMocks
    private CandidateController candidateController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(candidateController).build();
        jobs = new ArrayList<>(Arrays.asList
                ((new Jobs(10, "Engineer", "4year experience",
                                "2022-02-03", "2022,02,09", "Active")),
                        (new Jobs(18, "peon", "6year experience",
                                "2022-05-03", "2022,06,09", "Inactive")),
                        (new Jobs(11, "civil Engineer", "11year experience",
                                "2022-08-13", "2022,08,25", "Active"))));

    }

    @Test
    public void testFindAllJobs() throws Exception {
        when(jobService.getAllJobs()).thenReturn(jobs);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jpc/jobs/getAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].jobName", is("Engineer")));
    }

    @Test
    public void testFindActiveJobs() throws Exception {
        when(jobService.getAllJobs().stream().filter(jobs1 -> jobs1.getStatus().equals("active"))
                .collect(Collectors.toList())).thenReturn(jobs);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jpc/jobs/active")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].jobName", is("Engineer")));
    }

    @Test
    public void testFindInActiveJobs() throws Exception {
        when(jobService.getAllJobs().stream().filter(jobs1 -> jobs1.getStatus().equals("Inactive"))
                .collect(Collectors.toList())).thenReturn(jobs);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jpc/jobs/inactive")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].jobName", is("peon")));
    }

    @Test
    public void testFindJobsByIdActive() throws Exception {
        Jobs jobs = new Jobs(10, "Engineer", "4year experience",
                "2022-02-03", "2022,02,09", "Active");
        int jobId = 10;
        when(jobService.getJobsById(jobId)).thenReturn(jobs);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/jpc/jobs/get/10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
        assertEquals(JobConstants.ACTIVE.getValue(), candidateController.findJobsById(jobId).getBody().getStatus());
    }

    @Test
    public void testFindJobsByIdInActive() throws Exception {
        Jobs jobs = new Jobs(11, "Engineer", "4year experience",
                "2022-02-03", "2022,02,09", "Inactive");
        int jobId = 11;
        when(jobService.getJobsById(11)).thenReturn(jobs);
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/jpc/jobs/get/11")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        Assert.assertEquals("Inactive", jobs.getStatus());
    }

  /*  @Test
    public void testApplyJobs() throws Exception {
         Candidate candidate = new Candidate(14, "Nithin", "1990-10-10",
                95623225, "kollam", "nithin@gmail.com", 691560);
         Jobs jobs = new Jobs(10, "Engineer", "4year experience",
                 "2022-02-03", "2022,02,09", "Active");
        ResponseEntity<String> result = new ResponseEntity<>("Applied", HttpStatus.OK);
        when(jobRepository.findById(10)).thenReturn(Optional.of(jobs));
         Candidate candidate1 = new Candidate(14, "Nithin", "1990-10-10",
                95623225, "kollam", "nith@gmail.com", 691560);
        when(candidateService.apply(Candidate.class.newInstance())).thenReturn(candidate);
         ResponseEntity<String> result1 = candidateController.applyJobs(candidate1, 10);
        Assert.assertEquals(HttpStatus.OK, );
    assertThat(expectedResult).isEqualTo(expectedResult)
    }
    public void applyJobs()  {
        Candidate candidate = new Candidate(10,"Nithin","1996-19-09",
                95623274,"Kollam","nithin@gmail.com",691560);
        int jobId = 10;
       Jobs jobs = new Jobs(10, "Engineer", "4year experience",
               "2022-02-03", "2022,02,09", "Active");
       Mockito.when(jobRepository.findById(jobId).map(new Function<Jobs, Object>() {
           @Override
           public Object apply(Jobs jobs) {
               jobs.getCandidate().add(candidate);
               Mockito.when(jobRepository.findById(jobId)).thenReturn(Optional.of(jobs));

               Mockito.when(candidateService.apply(candidate)).thenReturn(candidate);
               String jsonRequest = null;
               try {
                   jsonRequest = objectMapper.writeValueAsString(jobs);
               } catch (JsonProcessingException e) {
                   throw new RuntimeException(e);
               }
               try {
                   CandidateControllerTest.this.mockMvc.perform(post("/jpc/jobs/10/apply")
                                   .content(jsonRequest)
                                   .contentType(MediaType.APPLICATION_JSON))
                           .andExpect(status().isCreated());
               } catch (Exception e) {
                   throw new RuntimeException(e);
               }


               return null;
           }


       }*/
    //));}
}