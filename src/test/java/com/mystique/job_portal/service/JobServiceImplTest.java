package com.mystique.job_portal.service;
import com.mystique.job_portal.entity.Jobs;
import com.mystique.job_portal.exception.JobNotFoundException;
import com.mystique.job_portal.repository.JobRepository;
import com.mystique.job_portal.service.impl.JobServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JobServiceImplTest {
    @Mock
    private JobRepository jobrepo;
    @InjectMocks
    private JobServiceImpl jobserviceimpl;
    @Test
    public void testAddJobs() {
        Jobs jobs = new Jobs(5,"Engineer","Mechanical engineering",
                "2022-08-05","2022-09-12","Active");
        when(jobrepo.save(jobs)).thenReturn(jobs);
        assertEquals(jobs,jobserviceimpl.addJobs(jobs));
    }
    @Test
    public void testGetAllJobs() {
        when(jobrepo.findAll()).thenReturn(Stream
                .of(new Jobs(10,"Engineer","4year experience",
                                "2022-02-03","2022,02,09","Active"),
                new Jobs(11,"Engineer","11year experience",
                "2022-08-13","2022,08,25","Active"))
                .collect(Collectors.toList()));
        assertEquals(2,jobserviceimpl.getAllJobs().size());
    }
   @Test
    public void testGetJobsById() throws JobNotFoundException {
      Jobs jobs = new Jobs(10,"Engineer","4year experience",
               "2022-02-03","2022,02,09","Active");
       int id =10;
       when(jobrepo.findById(id)).thenReturn(Optional.of(jobs));
       assertEquals(jobs.getJobId(),jobserviceimpl.getJobsById(id).getJobId());
    }
  @Test
  public void testUpdateJobs() throws JobNotFoundException {
      Jobs jobs = new Jobs(10,"Engineer","Mechanical engineering",
              "2022-08-05","2022-09-12","Active");
      when(jobrepo.findById(jobs.getJobId())).thenReturn(Optional.of(jobs));
      when(jobrepo.save(jobs)).thenReturn(jobs);
      assertEquals(jobs,jobserviceimpl.updateJob(jobs));
  }
  @Test
  public void testDeleteJob() throws JobNotFoundException {
       Jobs jobs = new Jobs(10,"Engineer","4year experience",
               "2022-02-03","2022,02,09","Active");
        when(jobrepo.findById(jobs.getJobId())).thenReturn(Optional.of(jobs));
        jobserviceimpl.deleteJob(jobs.getJobId());
        verify(jobrepo,times(1)).deleteById(jobs.getJobId());
    }
}