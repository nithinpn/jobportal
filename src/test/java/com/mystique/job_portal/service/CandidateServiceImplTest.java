package com.mystique.job_portal.service;

import com.mystique.job_portal.entity.Candidate;
import com.mystique.job_portal.repository.CandidateRepo;
import com.mystique.job_portal.service.impl.CandidateServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CandidateServiceImplTest {
    @InjectMocks
    private CandidateServiceImpl candidateServiceImpl;
    @Mock
    private CandidateRepo candidateRepo;

    @Test
    public void testApply() {
        Candidate candidate = new Candidate(10, "Nithin", "1996-19-09",
                95623274, "Kollam", "nithin@gmail.com", 691560);
        when(candidateRepo.save(candidate)).thenReturn(candidate);
        assertEquals(candidate, candidateServiceImpl.apply(candidate));
    }

}
