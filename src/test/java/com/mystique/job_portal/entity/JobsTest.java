package com.mystique.job_portal.entity;

import com.mystique.job_portal.commons.JobConstants;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class JobsTest {
    private Jobs jobs;

    @Before
    public void setUp(){
        jobs = new Jobs();
        jobs.setJobId(40);
        jobs.setJobName("Engineer");
        jobs.setJobDescription("10 year experience");
        jobs.setCreatedDate("2022-10-08");
        jobs.setLastModifiedDate("2022-11-06");
        jobs.setStatus("Active");
    }


    @Test
    public void testSetAndGetJobs() {
        assertEquals(40, jobs.getJobId());
        assertEquals("Engineer", jobs.getJobName());
        assertEquals("10 year experience" , jobs.getJobDescription());
        assertEquals("2022-10-08" , jobs.getCreatedDate());
        assertEquals("2022-11-06" , jobs.getLastModifiedDate());
        assertEquals("Active" , jobs.getStatus());
    }




}