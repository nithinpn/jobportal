package com.mystique.job_portal.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CandidateTest {

    private Candidate candidate;

    @Before
    public void setUp() {
        candidate = new Candidate();
        candidate.setCandidateId(20);
        candidate.setCandidateName("Nithin");
        candidate.setDateOfBirth("1996-09-19");
        candidate.setPhoneNumber(956232);
        candidate.setLocation("Kollam");
        candidate.setEmail("nithin@gmail.com");
        candidate.setZipCode(692563);
    }

    @Test
    public void testGetCandidate() {

        assertEquals(20, candidate.getCandidateId());
        assertEquals("Nithin", candidate.getCandidateName());
        assertEquals("1996-09-19", candidate.getDateOfBirth());
        assertEquals(956232, candidate.getPhoneNumber());
        assertEquals("Kollam", candidate.getLocation());
        assertEquals("nithin@gmail.com", candidate.getEmail());
        assertEquals(692563, candidate.getZipCode());
    }


}